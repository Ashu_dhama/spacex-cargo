import React, { Component } from 'react';
import { Navbar, Button, FormControl, NavbarBrand, NavLink } from 'react-bootstrap';
import jsonData from '../data/shipments.json';
import "./cargoPlanner.css"

class CargoPlanner extends Component {
    constructor(props) {
        super(props);
        this.state = {
            "completeData": jsonData,
            "searchString": "",
            "shipmentsData": [],
            "resultDiv": null,
            "error": null,
            "searchResult": null,
            "carGoVal": 0
        };
        this.loadData = this.loadData.bind(this);
        this.loadShipmentDataFromJson = this.loadShipmentDataFromJson.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
    }
    loadShipmentDataFromJson() {
        let shipmentsDatafromJson = jsonData.map((shipmentDetails, index) => {
            return shipmentDetails
        });
        this.setState ({
            shipmentsData : shipmentsDatafromJson
        });
    }
    loadData() {
        let result = this.state.shipmentsData.filter(data => {
            return data.name.match(this.state.searchString);
        });
        this.state ({
            searchResult : result
        })
    }

    changeHanler = name => event => {
        this.setState({
            [name]: event.target.value
        })
    }
    handleInputChange(event) {
        this.setState({
            [this.state.resultDiv.boxes] : event.target.value
        })
    }
    loadDetail = event => {
        event.preventDefault();
        let result = this.state.completeData.filter(data => {
            return data.name.match(event.target.getAttribute('href'));
        });
        let res = (result[0].boxes != null) ? result[0].boxes.split(',').map(function (v) { return +v }) : [0];
        const sum = res.reduce((result, number) => result + number);
        let cargoNewVal = Math.ceil(sum / 10);
        this.setState({ resultDiv: result[0], carGoVal: cargoNewVal });
    }

    render() {
        let shipmentDetailsData;
        if (this.state.resultDiv) {
            shipmentDetailsData = <div> <span><b>{this.state.resultDiv.name}</b> <br></br>
                <a href="#">{this.state.resultDiv.email} </a> <br></br>
                <h6>Number of Required bays  <b>{this.state.carGoVal}</b></h6>
                <h6>Cargo boxes</h6>
                <FormControl size="sm" value={this.state.resultDiv.boxes} onChange={this.handleInputChange} type="text" className="mr-sm-2" contentEditable />
            </span>
            </div>
        }
        return (
            <div className="App">
                <div>
                    <Navbar bg="light" expand="lg" aria-controls="responsive-navbar-nav">
                        <NavbarBrand>Cargo Planner</NavbarBrand> {' '}
                        <FormControl
                            value={this.state.searchString}
                            onChange={this.changeHanler('searchString')}
                            size="sm" type="text" placeholder="Search" className="mr-sm-2" />
                        <Button variant="primary" className="mr-sm-2" onClick={this.loadShipmentDataFromJson}>Load</Button>
                        <Button variant="primary">Save</Button>
                    </Navbar>
                </div>
                <div className="flex-container">
                    <div className="sidebar">
                        {this.state.shipmentsData.map((shipmentData) => {
                            return <NavLink href={shipmentData.name} onClick={this.loadDetail} key={shipmentData.id} data-value={shipmentData.id}> {shipmentData.name} </NavLink>
                        })}
                    </div>
                    <div className="shipment-details" >
                        {shipmentDetailsData}
                    </div>
                </div>
            </div>
        );
    }
}

export default CargoPlanner; 