import React from 'react';
import CargoPlanner from './component/cargoPlanner';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <CargoPlanner />
  );
}

export default App;